//
//  CafeListTableViewCell.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 17/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit

class CafeListTableViewCell: UITableViewCell {

    @IBOutlet weak var cafeTitleLabel: UILabel!
    @IBOutlet weak var distanceToCafeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(title: String, distance: Double?) {
        self.cafeTitleLabel.text = title
        guard let meters = distance else { return }
        self.distanceToCafeLabel.text = "\(Int(meters)) метров"
    }
    
}
