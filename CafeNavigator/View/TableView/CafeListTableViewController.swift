//
//  CafeListViewController.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 17/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit
import CoreLocation

protocol CafeListType: class {
    func routeToCafe(cafe: Features)
}

class CafeListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTableView: UITableView!
    
    var tableData = [Features]()
    weak var delegate: CafeListType!
    var userLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.register(UINib(nibName: "CafeListTableViewCell", bundle: nil), forCellReuseIdentifier: "CafeListTableViewCell")
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NetworkManager.shared.getInfoDataSet()
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "CafeListTableViewCell", for: indexPath) as? CafeListTableViewCell

        let cafeCoordinates = CLLocationCoordinate2D(latitude: tableData[indexPath.row].geometry?.coordinates?.last ?? 0, longitude: tableData[indexPath.row].geometry?.coordinates?.first! ?? 0)
        let distance = userLocation?.distance(from: CLLocation(latitude: cafeCoordinates.latitude, longitude: cafeCoordinates.longitude))
        
        cell?.setupCell(title: tableData[indexPath.row].properties?.attributes?.stationaryObjectName ?? "",
                        distance: distance)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.routeToCafe(cafe: tableData[indexPath.row])
    }

    
}
