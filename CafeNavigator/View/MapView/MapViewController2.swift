//
//  MapViewController.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 17/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
//import SwiftDate

class MapViewController2: UIViewController {
    
    @IBOutlet weak var countOfCafeLabel: UILabel!
    @IBOutlet weak var loadIndicatorLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var loadingIsProcess = false
    var currentCoordinats: CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var cafeList = [Features]() {
        didSet {
            self.setPin()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.delegate = self
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if (authorizationStatus == CLAuthorizationStatus.notDetermined) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true
        mapView.userTrackingMode = MKUserTrackingMode.none
        mapView.userLocation.title = ""
        
    }
    
    private func loadCafeList() {
        
        let coordinateString = "\(mapView.northWestCoordinate.longitude),\(mapView.northWestCoordinate.latitude),\(mapView.southEastCoordinate.longitude),\(mapView.southEastCoordinate.latitude)"
        self.loadIndicatorLabel.isHidden = false
        
        NetworkManager.shared.performNetworkRequest(with: .getCafeByGEO(coordinates: coordinateString)) { (responseData) in
            let result = try? JSONDecoder().decode(CafeItems.self, from: responseData)
            guard let cafeItems = result, let cafes = cafeItems.features else {
                self.loadIndicatorLabel.text = "Увеличьте масштаб карты"
                self.loadingIsProcess = false
                print("Ошибка парсинга json"); return }
            self.loadingIsProcess = false
            
            self.cafeList.appendDistinct(contentsOf: cafes, where: { (lhs, rhs) -> Bool in
                return lhs.properties?.attributes?.global_id != rhs.properties?.attributes?.global_id
            })
            
            self.loadIndicatorLabel.text = "Обновление данных"
            self.countOfCafeLabel.text = "Найдено \(self.cafeList.count) кафе с верандой"
            if self.cafeList.count > 0 {
                self.countOfCafeLabel.isHidden = false
                self.loadIndicatorLabel.isHidden = true
            } else {
                self.countOfCafeLabel.isHidden = true
                self.loadIndicatorLabel.isHidden = true
            }
        }
        
    }
    
    private func setPin() {
        DispatchQueue.main.async {  [weak self] in
            guard let cafeList = self?.cafeList else { return }
            for item in cafeList {
                
                let coordinates = CLLocationCoordinate2D(latitude: item.geometry?.coordinates?.last ?? 0, longitude: item.geometry?.coordinates?.first! ?? 0)
                let tag = item.properties?.attributes?.global_id ?? 0
                let annotation = MapAnnotation(coordinate: coordinates, title: item.properties?.attributes?.stationaryObjectName ?? "", subtitle: "", tag: tag)
                //                let annotation = MKPointAnnotation()
                
                //                annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                //                annotation.title = title
                
                let foo = self?.mapView.annotations.contains(where: { (annon) -> Bool in
                    if let myAnnotation = annon as? MapAnnotation {
                        return myAnnotation.tag == annotation.tag
                    } else {
                        return false
                    }
                    
                })
                
                if foo == false {
                    self?.mapView.addAnnotation(annotation)
                }
                
            }
        }
        
    }
    @IBAction func setMyLocation(_ sender: UIButton) {
        let myLocation = mapView.userLocation
        let viewRegion = MKCoordinateRegion(center: myLocation.coordinate, latitudinalMeters: CLLocationDistance(exactly: 300)!, longitudinalMeters: CLLocationDistance(exactly: 300)!)
        mapView.setRegion(viewRegion, animated: true)
    }
    
    private func saveCurrentPosition(_ coordinates: CLLocationCoordinate2D) {
        self.currentCoordinats = coordinates
    }
    
    private func needUpdateSendNewRequest(newCoordinates: CLLocationCoordinate2D) -> Bool {
        
        guard let currentCoordinats = currentCoordinats else { return true }
        let userLocation = CLLocation(latitude: newCoordinates.latitude, longitude: newCoordinates.longitude)
        let distance = userLocation.distance(from: CLLocation(latitude: currentCoordinats.latitude, longitude: currentCoordinats.longitude))
        
        if distance > 20 || distance == 0  { // when user move 20 meters away, send request to server
            saveCurrentPosition(newCoordinates)
            return true
        } else {
            return false
        }
    }
    
    private func getDirection(destinationCoordinate: CLLocationCoordinate2D) {
        guard let pickupCoordinate = mapView?.userLocation.coordinate else { return }
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
            
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
            let destinationAnnotation = MKPointAnnotation()
            
            if let location = destinationPlacemark.location {
                destinationAnnotation.coordinate = location.coordinate
            }
            
            self.mapView.showAnnotations([destinationAnnotation], animated: true )
            
        let directionRequest = MKDirections.Request()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .walking
            
            // Calculate the direction
            let directions = MKDirections(request: directionRequest)
            
            directions.calculate { [unowned self]
                (response, error) -> Void in
                
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    
                    return
                }
                
                let route = response.routes[0]
//                route.expectedTravelTime время в пути
                
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
                
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    
    private func loadData(userLocation: MKUserLocation) {
        if currentCoordinats == nil {
            if let coordinates = userLocation.location?.coordinate {
                saveCurrentPosition(coordinates)
                let distance = CLLocationDistance(exactly: 400)!
                let region = MKCoordinateRegion(center: coordinates, latitudinalMeters: distance, longitudinalMeters: distance)
                mapView.setRegion(region, animated: true)
            }
        }
        
        if !loadingIsProcess, needUpdateSendNewRequest(newCoordinates: userLocation.coordinate) {
            loadingIsProcess = true
            loadCafeList()
        }
    }
    
}

extension MapViewController2: CLLocationManagerDelegate, MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
        renderer.lineWidth = 5.0
        return renderer
    }

    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        loadData(userLocation: mapView.userLocation)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        loadData(userLocation: userLocation)
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? MapAnnotation {
            let features = cafeList.filter { (element) -> Bool in
                element.properties?.attributes?.global_id == annotation.tag
            }
            if !features.isEmpty {
                showMoreInforamtion(features.first!)
            } else {
                print("Ошибка! Информация о кафе не найдена в локальном массиве")
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        for view in self.children {
            if view is SlideUpViewController2 {
                view.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    private func showMoreInforamtion(_ cafe: Features) {
        let popUpMenu = SlideUpViewController2.init(delegate: self, cafeObj: cafe)
        self.present(popUpMenu, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                }
            }
        }
    }
}

extension MKMapView {
    
    var northWestCoordinate: CLLocationCoordinate2D { //*
        return MKMapPoint(x: visibleMapRect.minX, y: visibleMapRect.minY).coordinate
    }
    
    //    var northEastCoordinate: CLLocationCoordinate2D {
    //        return MKMapPoint(x: visibleMapRect.maxX, y: visibleMapRect.minY).coordinate
    //    }
    //
    var southEastCoordinate: CLLocationCoordinate2D { // *
        return MKMapPoint(x: visibleMapRect.maxX, y: visibleMapRect.maxY).coordinate
    }
    
    //    var southWestCoordinate: CLLocationCoordinate2D {
    //        return MKMapPoint(x: visibleMapRect.minX, y: visibleMapRect.maxY).coordinate
    //    }
    
}

public enum CalloutViewPlusBound {
    case defaultBounds
    case customBounds(CGRect)
}

public enum CalloutViewPlusCenter {
    case defaultCenter
    case customCenter(CGPoint)
}

extension MapViewController2: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let tableVC = viewController as? CafeListViewController {
            tableVC.tableData = self.cafeList
            tableVC.myTableView.reloadData()
            tableVC.delegate = self
            guard let userLocation = mapView.userLocation.location else { return }
            tableVC.userLocation = userLocation
        }
    }
    
}

extension MapViewController2: SlideUpMenuType {
    
    func showDirection(toPoint: CLLocationCoordinate2D) {
        mapView.removeOverlays(mapView.overlays)
        getDirection(destinationCoordinate: toPoint)
    }
    
}

extension MapViewController2: CafeListType {
    
    func routeToCafe(cafe: Features) {
        self.tabBarController?.selectedIndex = 0
        let coordinates = CLLocationCoordinate2D(latitude: cafe.geometry?.coordinates?.last ?? 0, longitude: cafe.geometry?.coordinates?.first! ?? 0)
        self.getDirection(destinationCoordinate: coordinates)
    }
    
}
