//
//  SlideUpViewController.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 22/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit
import CoreLocation

protocol SlideUpMenuType: class {
    func showDirection(toPoint: CLLocationCoordinate2D)
}

class SlideUpViewController2: UIViewController {

    private enum ViewTouchState {
        case began
        case moved
        case end, cancelled
    }
    
    @IBOutlet weak var mainView: UIView!
    weak var delegate: SlideUpMenuType!
    @IBOutlet weak var cafeTitleLabel: UILabel!
    @IBOutlet weak var cafeAdressLabel: UILabel!
    @IBOutlet weak var outSideView: UIView!
    
    var cafe: Features!
    
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)
    
    init(delegate: SlideUpMenuType, cafeObj: Features) {
        super.init(nibName: "SlideUpViewController2", bundle: nil)
        self.delegate = delegate
        self.modalPresentationStyle = .overCurrentContext
        self.cafe = cafeObj
        
//        let topView = AppDelegate().getTopMostViewController()
//        topView!.present(self, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 14
        // Do any additional setup after loading the view.
        setupView()
        
        outSideView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeView)))
    }
    
    @objc func closeView() {
        self.dismiss(animated: true, completion: nil)
    }

    private func setupView() {
        self.cafeTitleLabel.text = cafe.properties?.attributes?.stationaryObjectName ?? ""
        self.cafeAdressLabel.text = cafe.properties?.attributes?.address ?? ""
        
    }

    @IBAction func makeRoadToCafeBtn(_ sender: RoundButtonCustom) {
        let coordinates = CLLocationCoordinate2D(latitude: cafe.geometry?.coordinates?.last ?? 0, longitude: cafe.geometry?.coordinates?.first! ?? 0)
        
        delegate.showDirection(toPoint: coordinates)
        closeView()
    }
    

}



extension SlideUpViewController2: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension SlideUpViewController2: UIGestureRecognizerDelegate {
    
    private func touchHandler(state: ViewTouchState, touches: Set<UITouch>) {
        let touchPoint = touches.first?.location(in: self.view?.window)
        
        switch state {
        case .began:
            initialTouchPoint = touchPoint!
        case .moved:
            if touchPoint!.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint!.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        case .cancelled, .end:
            if touchPoint!.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .began, touches: touches)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .moved, touches: touches)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .end, touches: touches)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchHandler(state: .cancelled, touches: touches)
    }
    
}
