//
//  SponserBanner.swift
//  LinkYou
//
//  Created by Denis Tkachev on 01/03/2019.
//  Copyright © 2019 klen. All rights reserved.
//

import UIKit

class SponserBannerVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var pageTitleLabel: UILabel!
    @IBOutlet weak var closeViewBtn: UIButton!
    
    init() {
        super.init(nibName: "SponserBanner", bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
//        let topView = AppDelegate().getTopMostViewController()
//        topView!.present(self, animated: true, completion: nil) // можно через него делать презент и убрать
        
        }
    

    
    override func viewDidLoad() {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 3
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dismiss(animated: false, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressPayBtn(_ sender: UIButton) {
        guard let url = URL(string: "https://linkyou.ru/") else { return }
        UIApplication.shared.open(url)
    }
}

extension SponserBannerVC: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
