//
//  UIColor+zeplin.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 23/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var cerulean: UIColor {
        return UIColor(red: 0.0, green: 123.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
}

}
