//
//  MapAnnotation.swift
//  DD
//
//  Created by Alexander Lolov on 31.07.17.
//  Copyright © 2017 DomaDengi. All rights reserved.
//
import Foundation
import MapKit

class MapAnnotation: NSObject {
    var coordinate = CLLocationCoordinate2D()
    var title: String? = nil
    var subtitle: String?
    var tag: Int?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, tag: Int) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.tag = tag
    }
}

extension MapAnnotation:  MKAnnotation {
    
}
