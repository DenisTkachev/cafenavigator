
struct DataSetInfo : Codable {
	let id : Int?
	let identificationNumber : String?
	let categoryId : Int?
	let categoryCaption : String?
	let departmentId : Int?
	let departmentCaption : String?
	let caption : String?
	let description : String?
	let fullDescription : String?
	let keywords : String?
	let containsGeodata : Bool?
	let versionNumber : String?
	let versionDate : String?
	let itemsCount : Int?
	let columns : [Columns]?

	enum CodingKeys: String, CodingKey {

		case id = "Id"
		case identificationNumber = "IdentificationNumber"
		case categoryId = "CategoryId"
		case categoryCaption = "CategoryCaption"
		case departmentId = "DepartmentId"
		case departmentCaption = "DepartmentCaption"
		case caption = "Caption"
		case description = "Description"
		case fullDescription = "FullDescription"
		case keywords = "Keywords"
		case containsGeodata = "ContainsGeodata"
		case versionNumber = "VersionNumber"
		case versionDate = "VersionDate"
		case itemsCount = "ItemsCount"
		case columns = "Columns"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        identificationNumber = try values.decodeIfPresent(String.self, forKey: .identificationNumber)
		categoryId = try values.decodeIfPresent(Int.self, forKey: .categoryId)
		categoryCaption = try values.decodeIfPresent(String.self, forKey: .categoryCaption)
		departmentId = try values.decodeIfPresent(Int.self, forKey: .departmentId)
		departmentCaption = try values.decodeIfPresent(String.self, forKey: .departmentCaption)
		caption = try values.decodeIfPresent(String.self, forKey: .caption)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		fullDescription = try values.decodeIfPresent(String.self, forKey: .fullDescription)
		keywords = try values.decodeIfPresent(String.self, forKey: .keywords)
		containsGeodata = try values.decodeIfPresent(Bool.self, forKey: .containsGeodata)
		versionNumber = try values.decodeIfPresent(String.self, forKey: .versionNumber)
		versionDate = try values.decodeIfPresent(String.self, forKey: .versionDate)
		itemsCount = try values.decodeIfPresent(Int.self, forKey: .itemsCount)
		columns = try values.decodeIfPresent([Columns].self, forKey: .columns)
	}

}
