
struct Features : Codable {
	let geometry : Geometry?
	let properties : Properties?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case geometry = "geometry"
		case properties = "properties"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		geometry = try values.decodeIfPresent(Geometry.self, forKey: .geometry)
		properties = try values.decodeIfPresent(Properties.self, forKey: .properties)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}

//extension Features: Equatable {
//    static func == (lhs: Features, rhs: Features) -> Bool {
//        return lhs.properties?.attributes?.global_id != rhs.properties?.attributes?.global_id
//    }
//    
//}
