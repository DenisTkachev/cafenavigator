
struct Attributes : Codable {
	let objectType : String?
	let stationaryObjectName : String?
	let admArea : String?
	let district : String?
	let address : String?
	let facilityArea : Float?
	let global_id : Int?

	enum CodingKeys: String, CodingKey {

		case objectType = "ObjectType"
		case stationaryObjectName = "StationaryObjectName"
		case admArea = "AdmArea"
		case district = "District"
		case address = "Address"
		case facilityArea = "FacilityArea"
		case global_id = "global_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		objectType = try values.decodeIfPresent(String.self, forKey: .objectType)
		stationaryObjectName = try values.decodeIfPresent(String.self, forKey: .stationaryObjectName)
		admArea = try values.decodeIfPresent(String.self, forKey: .admArea)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		facilityArea = try values.decodeIfPresent(Float.self, forKey: .facilityArea)
		global_id = try values.decodeIfPresent(Int.self, forKey: .global_id)
	}

}
