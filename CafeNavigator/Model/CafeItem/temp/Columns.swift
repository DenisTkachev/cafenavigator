
struct Columns : Codable {
	let name : String?
	let caption : String?
	let visible : Bool?
	let type : String?
//    let subColumns : String?

	enum CodingKeys: String, CodingKey {

		case name = "Name"
		case caption = "Caption"
		case visible = "Visible"
		case type = "Type"
//        case subColumns = "SubColumns"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		caption = try values.decodeIfPresent(String.self, forKey: .caption)
		visible = try values.decodeIfPresent(Bool.self, forKey: .visible)
		type = try values.decodeIfPresent(String.self, forKey: .type)
//        subColumns = try values.decodeIfPresent(String.self, forKey: .subColumns)
	}

}
