
struct CafeItem : Codable {
	let global_id : Int?
	let number : Int?
	let cells : Cells?

	enum CodingKeys: String, CodingKey {

		case global_id = "global_id"
		case number = "Number"
		case cells = "Cells"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		global_id = try values.decodeIfPresent(Int.self, forKey: .global_id)
		number = try values.decodeIfPresent(Int.self, forKey: .number)
		cells = try values.decodeIfPresent(Cells.self, forKey: .cells)
	}

}
