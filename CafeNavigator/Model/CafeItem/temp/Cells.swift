
struct Cells : Codable {
	let global_id : Int?
	let objectType : String?
	let stationaryObjectName : String?
	let admArea : String?
	let district : String?
	let address : String?
	let facilityArea : Float?
	let geoData : Attributes?

	enum CodingKeys: String, CodingKey {

		case global_id = "global_id"
		case objectType = "ObjectType"
		case stationaryObjectName = "StationaryObjectName"
		case admArea = "AdmArea"
		case district = "District"
		case address = "Address"
		case facilityArea = "FacilityArea"
		case geoData = "geoData"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		global_id = try values.decodeIfPresent(Int.self, forKey: .global_id)
		objectType = try values.decodeIfPresent(String.self, forKey: .objectType)
		stationaryObjectName = try values.decodeIfPresent(String.self, forKey: .stationaryObjectName)
		admArea = try values.decodeIfPresent(String.self, forKey: .admArea)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		facilityArea = try values.decodeIfPresent(Float.self, forKey: .facilityArea)
		geoData = try values.decodeIfPresent(Attributes.self, forKey: .geoData)
	}

}
