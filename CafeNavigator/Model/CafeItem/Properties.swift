
struct Properties : Codable {
	let datasetId : Int?
	let versionNumber : Int?
	let releaseNumber : Int?
	let rowId : String?
	let attributes : Attributes?

	enum CodingKeys: String, CodingKey {

		case datasetId = "DatasetId"
		case versionNumber = "VersionNumber"
		case releaseNumber = "ReleaseNumber"
		case rowId = "RowId"
		case attributes = "Attributes"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		datasetId = try values.decodeIfPresent(Int.self, forKey: .datasetId)
		versionNumber = try values.decodeIfPresent(Int.self, forKey: .versionNumber)
		releaseNumber = try values.decodeIfPresent(Int.self, forKey: .releaseNumber)
		rowId = try values.decodeIfPresent(String.self, forKey: .rowId)
		attributes = try values.decodeIfPresent(Attributes.self, forKey: .attributes)
	}

}
