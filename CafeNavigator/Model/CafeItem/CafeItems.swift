
struct CafeItems : Codable {
	let features : [Features]?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case features = "features"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        features = try values.decodeIfPresent([Features].self, forKey: .features)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}

extension Array {
    public mutating func appendDistinct<S>(contentsOf newElements: S, where condition:@escaping (Element, Element) -> Bool) where S : Sequence, Element == S.Element {
        newElements.forEach { (item) in
            if !(self.contains(where: { (selfItem) -> Bool in
                return !condition(selfItem, item)
            })) {
                self.append(item)
            }
        }
    }
}
