//
//  NetworkManager.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 17/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import Moya

final class NetworkManager {
    let usersProvider = MoyaProvider<GeneralService>(plugins: [NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil)])
    
    static let shared = NetworkManager()
    
    public func getInfoDataSet() { // удалить - перенести в метод performNetworkRequest
        usersProvider.request(.infoDataSet) { (result) in
            switch result {
            case .success(let response):
                do {
                    let result = try JSONDecoder().decode(DataSetInfo.self, from: response.data)
                    print(result)
                    
                    
                } catch {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    public func performNetworkRequest(with: GeneralService, completion: @escaping (Data)->()) {
        
        usersProvider.request(with) { (result) in
            
            switch result {
            case .success(let response):
                completion(response.data)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
}
