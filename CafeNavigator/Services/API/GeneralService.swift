//
//  GeneralService.swift
//  CafeNavigator
//
//  Created by Denis Tkachev on 17/05/2019.
//  Copyright © 2019 Denis Tkachev. All rights reserved.
//

import Moya

enum GeneralService {
    case infoDataSet
    case getCafeList
    case getCafeByGEO(coordinates: String)
}

extension GeneralService: TargetType {
    var baseURL: URL {
        return URL(string: "https://apidata.mos.ru/v1/")!
    }
    
    var path: String {
        switch self {
        case .infoDataSet:
            return "/datasets/\(Config.shared.dataSet)/"
        case .getCafeList:
            return "/datasets/\(Config.shared.dataSet)/rows/"
        case .getCafeByGEO:
            return "/datasets/\(Config.shared.dataSet)/features/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .infoDataSet, .getCafeList, .getCafeByGEO:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .infoDataSet:
            return .requestParameters(parameters: ["api_key" : Config.shared.apiKey], encoding: URLEncoding.default)
            
        case .getCafeList:
            return .requestParameters(parameters: ["api_key" : Config.shared.apiKey, "$top" : 100], encoding: URLEncoding.default)
        
        case .getCafeByGEO(let coordinates):
            return .requestParameters(parameters: ["api_key" : Config.shared.apiKey, "bbox" : coordinates], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type" : "application/json; charset=UTF-8"]
    }
}

extension GeneralService: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType {
        return .none
    }
}
